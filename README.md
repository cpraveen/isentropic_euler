# Finite volume code for isentropic Euler

A 2-d Cartesian grid finite volume code for isentropic Euler equations with periodic boundary conditions.
